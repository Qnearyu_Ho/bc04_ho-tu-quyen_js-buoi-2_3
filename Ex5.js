/*
INPUT:
Nhập vào số bất kì n 

TODO:
Tìm số ở hàng đơn vị 
unit = n % 10 
Tìm số ở hàng chục
ten = Math.floor(n%100/10)
Tổng của 2 kí số
sum = ten + unit;

OUTPUT:
=> hiển thị ra sum 
 */
function tinhTongHaiKySo(){
    var n = document.getElementById("so").value*1;
    var unit = n % 10;
    var ten = Math.floor(n%100/10);
    var sum = ten + unit;
    document.getElementById("result").innerHTML = `<p> Tổng 2 ký số của số vừa nhập là: ${sum} </p>`

}
