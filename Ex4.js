/*
INPUT:
Nhập vào chiều dài a
Nhập vào chiều rộng b 
TODO:
Diện tích hình chữ nhật S = a * b
Chu vi hình chữ nhật C = (a + b) * 2
OUTPUT: Kết quả
S =
C =
 */
function tinh(){
    var a = document.getElementById("dai").value*1;
    var b = document.getElementById("rong").value*1;
    var S = a * b;
    var C = (a + b) * 2;
    document.getElementById("result").innerHTML = `<p> Diện tich hình chữ nhật là: ${S}<br/>
    Chu vi hình chữ nhật là: ${C} </p>`;

}
