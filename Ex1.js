/*
INPUT: Nhập vào lương 1 ngày

TODO: 
Nhập vào số ngày làm n
Tính tiền lương bằng công thức
totalSalary = Salary_1day * n

OUTPUT: Kết quả sau khi tính
=> hiển thị result

*/ 
function tinhTienLuong(){
    var Salary_1day = document.getElementById("luong1ngay").value*1;
    var n = document.getElementById("soNgay").value*1;

    var totalSalary = Salary_1day * n;
    document.getElementById("result").innerHTML = `<p> Tiền lương của nhân viên là: ${totalSalary} VND`;

}
